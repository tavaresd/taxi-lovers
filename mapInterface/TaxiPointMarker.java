package mapInterface;

import java.awt.Color;

import taxiData.TaxiLovers;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.marker.SimplePointMarker;

/**
 * TaxiMarker.java
 * extends AbstractMarker
 * @author Sean O'Carroll
 * Modified Marker Class to change 
 */
public class TaxiPointMarker extends SimplePointMarker {

	public TaxiPointMarker(Location l0, int noOfTaxis, float radius) {
		super(l0);	
		int r = (int)((255 * noOfTaxis) / 100);
		int g = (int)((255 * (100 - noOfTaxis)) / 100 );
		this.color = TaxiLovers.mainApplet.color(r, g, 0, 150);
		this.radius = radius;
		this.strokeWeight = 0;
	}
	public TaxiPointMarker(Location l0) {
		super(l0);	
		Color thisCol = new Color(0);
		this.color = thisCol.getRGB();
		this.strokeWeight = 0;
	}
	
	

}
