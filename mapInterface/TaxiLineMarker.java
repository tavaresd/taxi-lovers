package mapInterface;

import taxiData.DataContainer;
import taxiData.TaxiLovers;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.marker.SimpleLinesMarker;

/**
 * TaxiLineMarker.java
 * extends SimpleLinesMarker
 * @author Sean O'Carroll
 * Specialized Line Marker class that changes colour based on distance traveled
 */
public class TaxiLineMarker extends SimpleLinesMarker {
	
	public TaxiLineMarker(DataContainer journey, double longest) {
		super(new Location(journey.getPickupCoordinates().getLatitude(),
				journey.getPickupCoordinates().getLongitude()),
				new Location(journey.getDropOffCoordinates().getLatitude(),
						journey.getDropOffCoordinates().getLongitude()));
		
		double adj = 255 - (journey.getTripDistance()/longest)*255;
		
		int r = (int)adj;
		
		this.color = TaxiLovers.mainApplet.color(r, r, r);
		this.strokeWeight = 1;
	}
	
	/**
	 * @author Dan Ormsby
	 */
	public TaxiLineMarker(DataContainer journey, double longest, int sw) {
		this(journey, longest);
		this.strokeWeight = sw;
	}
	public TaxiLineMarker(double x, double y, double x2, double y2) {
		super(new Location(y,x),new Location(y2,x2));		
		
		this.color = 0xFFFF0000;
		this.strokeWeight = 1;
	}
	public TaxiLineMarker(double x, double y, int sw) {
		super(new Location(y,x),new Location(40.6397,-73.7789));		
		int a = (int) TaxiLovers.mainApplet.random(255);
		int b = (int) TaxiLovers.mainApplet.random(255);
		int c = (int) TaxiLovers.mainApplet.random(255);
		this.color = TaxiLovers.mainApplet.color(a, b, c, 128);
		this.strokeWeight = sw;
	}
	
}
