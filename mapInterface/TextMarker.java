package mapInterface;

import processing.core.PApplet;
import processing.core.PGraphics;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.marker.AbstractMarker;

public class TextMarker extends AbstractMarker {
	
	private String s;

	public TextMarker(Location l, String text, int colour) {
		super(l);
		s = text;
		this.color = colour;
	}
	
	@Override
	public void draw(PGraphics pg, float x, float y) {
		pg.pushStyle();
	    pg.noStroke();
	    float t = pg.textSize;
	    pg.fill(color);
	    pg.textSize(t * 1.2f);
	    pg.textAlign(PApplet.CENTER);
	    pg.text(s, x, y);
	    pg.textSize(t);
	    pg.popStyle();
	}

	@Override
	protected boolean isInside(float arg0, float arg1, float arg2, float arg3) {
		return false;
	}
	
}
