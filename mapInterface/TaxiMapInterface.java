package mapInterface;

import java.util.ArrayList;

import taxiData.DataContainer;
import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.marker.SimplePointMarker;
import de.fhpotsdam.unfolding.geo.Location;

/**
 * TaxiMapInterface.java
 * @author Sean O'Carroll
 * @modified Dan Ormsby, added strokeWeight
 * Class to hold useful static methods
 */
public class TaxiMapInterface {

	public static void addJourneys(UnfoldingMap map, ArrayList<DataContainer> journeys, int strokeWeight) {
		double longestTrip = 0;
		for (DataContainer dc : journeys) {
			if (dc.getTripDistance() > longestTrip) {
				longestTrip = dc.getTripDistance();
			}
		}
		for (int i=0; i<journeys.size(); i++) {
			DataContainer journey = journeys.get(i);
			TaxiLineMarker m0 = new TaxiLineMarker(journey, longestTrip, strokeWeight);
			TaxiPointMarker m1 = new TaxiPointMarker(journey.getPickupLocation(), journey.getPassengerCount(), journey.getRateCode());
			SimplePointMarker m2 = new SimplePointMarker(journey.getDropOffLocation());
			map.addMarkers(m0, m1, m2);
		}
	}
	public static void addJourneys(UnfoldingMap map, Location JFK, double x, double y, int strokeWeight) {
		Location taxiLo = new Location(40.6397, -73.7789);
		TaxiLineMarker m0 = new TaxiLineMarker(x, y, strokeWeight);
		TaxiPointMarker m1 = new TaxiPointMarker(JFK);
		TaxiPointMarker m2 = new TaxiPointMarker(taxiLo);
		map.addMarkers(m0, m1, m2);
	}
}
