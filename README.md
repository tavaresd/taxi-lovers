#taxi-lovers

Groundbreaking research into NYC taxi data by a lovable, rag-tag bunch of taxi enthusiasts.

Instructions to setup environment to support this project:
( adapted from https://processing.org/tutorials/eclipse/ )

1) Download and install Eclipse

2) Import the Processing libraries
    FILE -> IMPORT -> GENERAL -> FILESYSTEM

    Browse to where Processing is installed on the given PC, as said:
        On Windows: PATH_TO_PROCESSING/core/library/core.jar
        On OSX:     /Applications/Processing.app/Contents/Java/core/library/core.jar

3) Click finish

4) The jar should now be appearing in package explorer, right-click the file and select "BUILD PATH -> ADD TO BUILD PATH"

Instructions to run the program:
    RUN -> RUN AS -> JAVA APPLET

You will also need to import the libraries in the javaLib/ folder. These can be added to the project by right clicking "Referenced Libraries > Build Path > Configure Build Path > Add External Jars" and then selecting all the jars in that folder.

Resources (the main screen and search backgrounds) need to be placed in the bin/ folder within the Eclipse project, as this is where Processing searches for them.


It should now be running as intended.