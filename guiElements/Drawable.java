package guiElements;

/**
 * Drawable.java
 * @author D�rio Tavares Antunes
 * An interface used in the screen class to simplify drawing calls for 
 * specialised objects (Graph, HullProducer, etc.)
 */
public interface Drawable {

	public void draw();

}
