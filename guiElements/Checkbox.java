package guiElements;

import processing.core.PApplet;
import taxiData.Events;

/**
 * Checkbox.java
 * @author Patrick O'Boyle
 * Class used to create checkboxes, extending Widget.java
 */

public class Checkbox extends Widget {
    boolean checked;
    
    /**
     * Creates a Widget object with the given attributes.
     * @param x
     * @param y
     * @param width
     * @param label
     * @param label
     * @param event
     * @author Patrick O'Boyle
     * @modified D�rio Tavares Antunes
     */
    public Checkbox(int x, int y, int width, String label, int event) {
        super(x, y, width, width, label, 0, 0, event);
        this.width = width; //has to override the widget fitting the box to its size

        checked = true;
    }

    /**
     * method draw
     * Called periodically by Processing, writes the currently selected entry.
     * to the screen.
     * @author Patrick O'Boyle
     */
    public void draw() {
        if (!checked) {
            parent.fill(255);
            parent.rect(x, y, width, height);
            parent.textAlign(PApplet.LEFT);
            parent.text(label, x + width + 10, y + height);
        } else {
            parent.fill(0);
            parent.rect(x, y, width, height);
            parent.fill(255);
            parent.textAlign(PApplet.LEFT);
            parent.text(label, x + width + 10, y + height);
        }
    }
    
    /**
     * method getEvent
     * Returns the event number of this widget, if it has been clicked.
     * @author Patrick O'Boyle
     */
    public int getEvent(int mX, int mY){
    	if(mX > x && mX < x + width && mY > y && mY <y + height){
    		checked = !checked;
    		return event;
    	}
    	return Events.INVALID;
    }

    /**
     * method isChecked
     * Returns whether the checkbox is checked or unchecked.
     * @author Patrick O'Boyle
     */
    public boolean isChecked(){
        return checked;
    }


}
