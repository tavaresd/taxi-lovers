package guiElements;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PImage;
import taxiData.Events;
import taxiData.TaxiLovers;
import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.events.EventDispatcher;
import de.fhpotsdam.unfolding.events.PanMapEvent;
import de.fhpotsdam.unfolding.events.ZoomMapEvent;
import de.fhpotsdam.unfolding.utils.MapUtils;

/**
 * Screen.java
 * @author Nicolas Cardozo
 * @modified Dan Ormsby
 * @modified Patrick O'Boyle
 * @modified Joseph Fitpatrick
 * @modified D�rio Tavares Antunes
 * Class used draw the main screen.
 */

public class Screen implements Drawable, Interactable {

	private static PApplet parent = TaxiLovers.mainApplet;
	private int screenColor;
	private PImage bgImage;
	private UnfoldingMap map;
	private static int[] rgbaMask = new int[] {0xFF0000, 0xFF00, 0xFF, 0xFF000000};
	private EventDispatcher events;
	private ArrayList<Widget> screenWidgets = new ArrayList<>();
	private Interactable interact;
	private Drawable draw;

	/**
	 * Creates a new Screen object, with drawable and interactive specialised
	 * objects and an UnfoldingMap.
	 * @param i, d
	 * The specialised drawable/interactable objects.
	 * @param map
	 * A map, to be registered/deregistered for interaction.
	 * @author D�rio Tavares Antunes
	 */
	public Screen(Interactable i, Drawable d, UnfoldingMap map) {
		interact = i;
		draw = d;
		if (map != null)
			events = MapUtils.createDefaultEventDispatcher(parent, map);
		this.map = map;
		deregister();
	}

	/**
	 * Creates a new Screen object, with drawable and interactive specialised
	 * objects.
	 * @param i, d
	 * The specialised drawable/interactable objects.
	 * @author D�rio Tavares Antunes
	 */
	public Screen(Interactable i, Drawable d) {
		this(i, d, null);
	}
	/**
	 * Creates a screen which uses drawable to create screen
	 * @param d
	 * @author Joseph Fitzpatrick
	 */
	public Screen(Drawable d) {
		this(null, d, null);
	}

	/**
	 * Creates a Screen object with the given attributes.
	 * @param colour
	 * The colour the background will be coloured.
	 * @author Nicolas Cardozo
	 * @modified Dan Ormsby
	 */

	public Screen(int colour){
		this.screenColor = colour; 
	}

	/**
	 * Creates a new Screen object with a given background image.
	 * @param bgImage
	 * @param parent
	 * @author D�rio Tavares Antunes
	 */
	public Screen(PImage bgImage) {
		this.bgImage = bgImage;
		this.bgImage.resize(parent.width, parent.height);
	}

	/**
	 * Creates a new Screen object which draws a given UnfoldingMap
	 * @param map
	 * @param parent
	 * @author D�rio Tavares Antunes
	 * @param e 
	 */
	public Screen(UnfoldingMap map) {
		this(null, null, map);
	}

	/**
	 * method add
	 * Adds the passed widget to the screenWidgets ArrayList.
	 * @param w
	 * @author Nicolas Cardozo
	 * @modified Dan Ormsby
	 * @modified Patrick O'Boyle
	 */
	public void add(Widget	w){	
		screenWidgets.add(w);	
	}

	public void remove(Widget w) {
		screenWidgets.remove(w);
	}

	/**
	 * method draw
	 * Called periodically by Processing, draws the contents of this Screen
	 * object to the screen.
	 * @author Nicolas Cardozo
	 * @modified Dan Ormsby
	 * @modified Patrick O'Boyle
	 * @author D�rio Tavares Antunes
	 */
	public void draw() {
		if (draw != null) {
			draw.draw();
		} else if (bgImage != null) {
			parent.background(bgImage);
		} else if (map != null){
			map.draw();
		} else {
			parent.background(screenColor & rgbaMask[0],
					screenColor & rgbaMask[1],
					screenColor & rgbaMask[2],
					screenColor & rgbaMask[3]);
		}

		for(int	i =	0; i < screenWidgets.size(); i++){
			screenWidgets.get(i).draw();	
		}	
	}

	/**
	 * method getEvent
	 * Returns the event number of this widget, if it has been clicked.
	 * @author Nicolas Cardozo
	 * @modified Dan Ormsby
	 */
	public int getEvent(int mx, int my) {
		int iEvent = interact == null ? Events.INVALID : interact.getEvent(mx, my);
		if (iEvent != Events.INVALID)
			return iEvent;
		for(int	i = 0; i < screenWidgets.size(); i++){
			int	event =	screenWidgets.get(i).getEvent(mx, my);	

			if(event !=	Events.INVALID){
				return event;	
			}	
		}

		return Events.INVALID;
	}

	/**
	 * method deregister
	 * If the Screen has control of an UnfoldingMap, deregisters it for
	 * interaction, so it does not react to button presses while it is not on
	 * screen.
	 * @author D�rio Tavares Antunes
	 */
	public void deregister() {
		if (map != null && events != null) {
			events.unregister(map, PanMapEvent.TYPE_PAN, map.getId());
			events.unregister(map, ZoomMapEvent.TYPE_ZOOM, map.getId());
		}
	}

	/**
	 * method register
	 * If the Screen has control of an UnfoldingMap, registers it for
	 * interaction, so it reacts to button presses while it is on screen.
	 * @author D�rio Tavares Antunes
	 */
	public void register() {
		if (map != null && events != null) {
			events.register(map, PanMapEvent.TYPE_PAN, map.getId());
			events.register(map, ZoomMapEvent.TYPE_ZOOM, map.getId());
		}
	}

	public ArrayList<Widget> getWidgets(){
		return screenWidgets;
	}

	@Override
	/**
	 * method mousePressed
	 * Called when the mouse is pressed, the call is passed on to an interactive
	 * specialised object if the Screen object contains one. 
	 * @author D�rio Tavares Antunes
	 */
	public void mousePressed() {
		if (interact != null)
			interact.mousePressed();
	}

	@Override
	/**
	 * method mouseMoved
	 * Called when the mouse is moved, the call is passed on to an interactive
	 * specialised object if the Screen object contains one. 
	 * @author D�rio Tavares Antunes
	 */
	public void mouseMoved() {
		if (interact != null)
			interact.mouseMoved();
	}

}
