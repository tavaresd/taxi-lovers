package guiElements;

import processing.core.*;
import taxiData.C;
import taxiData.Events;
import taxiData.TaxiLovers;

/**
 * Widget.java
 * @author Nicolas Cardozo
 * @modified Dan Ormsby
 * @modified Patrick O'Boyle
 * Class used to create widgets.
 */

public class Widget {
	protected int x, y, width, height, colour, cornerColour;
	protected String label;
	protected static PApplet parent = TaxiLovers.mainApplet;
	protected int event;
	private float textSize;

	/**
	 * Creates a Widget object with the given attributes.
	 * @param x
	 * The distance in pixels the button's left edge is from the left of the
	 * screen.
	 * @param y
	 * The distance in pixels the button's top edge is from the top of the screen.
	 * @param width
	 * The width of the button, including edges.
	 * @param height
	 * The height of the button, including edges.
	 * @param label
	 * The text to display on the button.
	 * @param colour
	 * The colour the top two thirds of the button is coloured.
	 * @param event
	 * The event to return if this button is clicked on.
	 * @author Nicolas Cardozo
	 * @modified Dan Ormsby
	 * @modified Patrick O'Boyle
	 * @modified D�rio Tavares Antunes
	 */
	public Widget(int x, int y, int width, int height, String label, int colour,
			int cornerColour, int event) {
		this.x = x;
		this.y = y;

		this.height	= height;
		this.label = label;
		this.event = event;
		this.colour = colour;
		
		// The corners have a fixed transparency, which is applied here.
		this.cornerColour = (cornerColour & 0xFFFFFF) + 0xAA000000;
		textSize = findTextSize(5, height/3);
		
		// Makes sure the button has at least the minimum width allowed to
		// display the text it contains.
		if (parent.textWidth(label) > width - 29) {
			width = (int) parent.textWidth(label) + 30;
		}
		this.width = width;
	}
	
	/**
	 * method findTextSize
	 * Tries to find a good size for text. 
	 * @param low
	 * The minimum wanted text size.
	 * @param height
	 * The height the text should be at least.
	 * @return 
	 * The size to set the text to.
	 * @author D�rio Tavares Antunes
	 */
	private float findTextSize(float low, float height) {
		parent.textSize(low);
		while (parent.textAscent() - height < 0) {
			low += 2;
			parent.textSize(low);
		}
		return low;
	}

	/**
	 * method draw
	 * Called periodically by Processing, draws a button to the square with
	 * coloured corners as defined, the top two-thirds of the button set to the
	 * colour defined and a strip of black and yellow boxes to look like a taxi
	 * pattern in the bottom third, with the text baseline set to the top of the
	 * bottom third of the button.  
	 * @author D�rio Tavares Antunes
	 */
	public void draw() {
		parent.noStroke();
		int innerX = x + C.BUTTON_CORNER_WIDTH;
		int innerWidth = width - 2 * C.BUTTON_CORNER_WIDTH;
		int innerY = y + C.BUTTON_CORNER_WIDTH;
		int innerHeight = height - 2 * C.BUTTON_CORNER_WIDTH;
		
		// Draw the corners
		parent.fill(cornerColour);
		for (int i = 0; i < 2; i++) {
			parent.rect(x + (3 * width * i)/4, y, width / 4, height/4);
			parent.rect(x + (3 * width * i)/4, y + (3 * height)/4, width/4, height/4);
		}
		
		// Draw the center
		parent.fill(colour);
		parent.rect(innerX, innerY, innerWidth, innerHeight);
		
		// Draw the taxi strip along the bottom
		float boxWidth = innerHeight/6;
		int boxesFitted = (int) (innerWidth/boxWidth);
		float yBase = innerY + innerHeight;
		for (int i = 0; i < boxesFitted + 1; i++) {
			for (int j = 0; j < 2; j++) {
				if ((i + j) % 2 == 0) {
					parent.fill(255, 255, 0);
				} else {
					parent.fill(0, 0, 0);
				}
				if (i != boxesFitted) {
					parent.rect(innerX + (i * boxWidth), yBase - ((j + 1) * boxWidth), boxWidth, boxWidth);
				} else {
					parent.rect(innerX + (i * boxWidth), yBase - ((j + 1) * boxWidth), innerWidth - i * boxWidth, boxWidth);
				}
			}
		}
		
		// Write text on top of the taxi strip
		parent.fill(255);
		parent.textAlign(PApplet.CENTER);
		parent.textSize(textSize);
		parent.text(label, innerX + innerWidth/2, innerY + (2*innerHeight)/3);
	}
	
	/**
	 * method getEvent
	 * Returns the event number of this widget, if it has been clicked.
	 * @author Nicolas Cardozo
	 * @modified Dan Ormsby
	 */
	public int getEvent(int mX, int	mY){	
		if(mX > x && mX	< x + width && mY > y && mY < y + height){
			return event;	
		}

		return Events.INVALID;	
	}

	/**
	 * method getLabel
	 * Returns the label of the widget
	 * @author Patrick O'Boyle
	 */
	public String getLabel(){
		return label;
	}
	
	/**
	 * method getWidth
	 * @return
	 * The width of this widget, useful for self-resizing widgets.
	 * @author D�rio Tavares Antunes
	 */
	public int getWidth() {
		return width;
	}
	
}