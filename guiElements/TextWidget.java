package guiElements;

import processing.core.PApplet;
import taxiData.Events;

public class TextWidget extends Widget {
	
	private int textSize = 5;

	public TextWidget(int x, int y, int maximumWidth, String label, int colour) {
		super(x, y, 0, 0, label, colour, 0, Events.INVALID);
		parent.textSize(textSize);
		while (parent.textWidth(label) < maximumWidth) {
			textSize++;
			parent.textSize(textSize);
		}
		textSize--;
		parent.textSize(textSize);
	}
	
	public void draw() {
		parent.fill(colour);
		parent.textAlign(PApplet.CENTER);
		parent.textSize(textSize);
		parent.text(label, x, y);
	}

}
