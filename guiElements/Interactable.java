package guiElements;

/**
 * Interactable.java
 * @author D�rio Tavares Antunes
 * An interface used in the screen class to simplify interaction for 
 * specialised objects (Graph, HullProducer, etc.)
 */
public interface Interactable {
	
	public void mousePressed();
	public void mouseMoved();
	public int getEvent(int mouseX, int mouseY);

}
