package guiElements;

import processing.core.PApplet;
import taxiData.Events;

/**
 * RadioButton.java
 * @author Patrick O'Boyle
 * Class used to create Radio Buttons.
 */
public class RadioButton extends Widget{

    boolean checked;
    boolean checked2;

    int event2;
    String label2;

    int x2, y2;

    /**
     * Creates a RadioButton object with the given attributes.
     * @param x
     * @param y
     * @param x2
     * @param y2
     * @param width
     * @param height
     * @param label
     * @param label2
     * @param event1
     * @param event2
     * @author Patrick O'Boyle
     */
    public RadioButton(int x,
                int y,
                int x2,
                int y2,
                int width,
                int height,
                String label,
                String label2,
                int event1,
                int event2)
    {
        super(x, y, width, height, label, 0, 0, event1);
        // Bypass the widget's restrictions on width.
        this.width = width;
        this.height = height;

        this.x2 = x2;
        this.y2 = y2;

        this.label2 = label2;

        checked = true;
        checked2 = false;

        this.event2 = event2;

    }

    /**
     * method draw
     * Called periodically by Processing, writes the currently selected entry.
     * to the screen.
     * @author Patrick O'Boyle
     */
    public void draw(){
        if(!checked){
            parent.fill(255);
            parent.rect(x, y, width, height);
        }else{
            parent.fill(0);
            parent.rect(x, y, width, height);
        }
        if(!checked2){
            parent.fill(255);
            parent.rect(x2, y2, width, height);
        }else{
            parent.fill(0);
            parent.rect(x2, y2, width, height);
        }

        parent.fill(255);
        parent.textAlign(PApplet.LEFT);
        parent.text(label, x + width + 10, y + height);
        parent.text(label2, x2 + width + 10, y2 + height);
    }

    /**
     * method getEvent
     * Returns the event number of this widget, if it has been clicked.
     * @author Patrick O'Boyle
     */
    public int getEvent(int mX, int mY){
        if(mX>x && mX < x+width && mY >y && mY <y+height){
            checked = !checked;
            if(checked){
                checked2 = false;
            }
            return event;
        }

        if(mX> x2 && mX < x2 + width && mY > y2 && mY < y2 +height){
            checked2 = !checked2;
            if(checked2){
                checked = false;
            }
            return event2;
        }

        return Events.INVALID;
    }
}