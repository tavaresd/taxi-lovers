package dbInterface;

import guiElements.Checkbox;
import guiElements.Widget;

import java.util.ArrayList;

/**
 * Utility.java
 * @author Patrick O'Boyle
 */
public class Utility {

	private final static int[] RATE_CODES = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 28, 65, 128, 210};
	
    private final static String SELECT = "SELECT * FROM TAXIS";
    private final static String WHERE = " WHERE ";
    private final static String AND = " AND ";

    /**
     * Creates a query string to query the database, using an array of strings
     * @param conditions
     * @author Patrick O'Boyle
     */
    public static String generateQuery(String[] conditions){

        // Used to efficiently create the query String
        StringBuilder builder = new StringBuilder(SELECT);

        // If there are no conditions, WHERE will not be appended
        // And all items will be returned
        if(conditions != null && conditions.length != 0) {
            builder.append(WHERE);

            // The first condition will be added.
            // For each following condition, AND + [Condition] will be appended.
            for (int i = 0; i < conditions.length; i++) {
                builder.append(conditions[i]);

                // If there is a following condition, append AND
                if(i + 1 < conditions.length){
                    builder.append(AND);

                }
            }
        }
        return builder.toString();
    }

    /**
     * Creates a query string to query the database, using a String
     * @param conditions
     * @author Patrick O'Boyle
     * @modified D�rio Tavares Antunes
     */
    public static String generateQuery(String conditions){
        // Used to efficiently create the query String
        StringBuilder builder = new StringBuilder(SELECT);

        // If there are no conditions, WHERE will not be appended
        // And all items will be returned
        if(!conditions.equals("")) {
            builder.append(conditions);
        }
        String s = builder.substring(0, builder.length() - 1);

        s += ");";

        return s;
    }


    /**
     * Creates a query string to query the database
     * @param widgets
     * @author Patrick O'Boyle
     */
    public static String generateConditions(ArrayList<Widget> widgets){
        ArrayList<Boolean> values = new ArrayList<>();

        // Checks the value of each checkbox, storing whether it's checked in an arraylist of booleans
        for(int i = 2; i < widgets.size() - 1; i++){
        	if (widgets.get(i) instanceof Checkbox) {
        		Checkbox c = (Checkbox) widgets.get(i);
        		if(c.isChecked())
        			values.add(true);
        		else
        			values.add(false);
        	}
        }

        boolean CMT, VTS;

        if(((Checkbox)widgets.get(2)).isChecked()) {
            CMT = true;
            VTS = false;
        }else {
            CMT = false;
            VTS = true;
        }
        
        String conditions = " WHERE";

        if (CMT && !VTS)
            conditions += " VENDORID = 'CMT' ";
        else if(VTS)
            conditions += " VENDORID = 'VTS' ";

        boolean firstElement = true;

        for(int i = 0; i < 14; i++) {
        	if (firstElement && values.get(i)) {
        		conditions += "AND (RATECODE = " + RATE_CODES[i] + " ";
        		firstElement = false;
        	} else if(values.get(i))
        		conditions += "OR RATECODE = " + RATE_CODES[i] + " ";
        }
        conditions += ")";
        return conditions;
    }

}
