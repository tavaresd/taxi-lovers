package dbInterface;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import taxiData.DataContainer;

/**
 * DatabaseHandle.java
 * @author D�rio Tavares Antunes
 * Connects and handles a single database file.
 */
public class DatabaseHandle implements Runnable {

	private Connection handle;
	private String query;

	/**
	 * method run
	 * Called when this object's thread starts.
	 * Queries its database file, processes the result into usable DataContainer
	 * objects and then signals the DatabasInterface that it has completed.
	 * @author D�rio Tavares Antunes
	 */
	@Override
	public void run() {
		try {
			// Queries its database file, then converts the returned results
			// into DataContainers which are returned.
			ResultSet results = handle.createStatement().executeQuery(query);
			
			ArrayList<DataContainer> processedResults = new ArrayList<>();
			while (results.next()) {
				DataContainer d = new DataContainer(results.getString(2), results.getString(3),
						results.getString(4), results.getInt(5), results.getString(6), 
						results.getLong(7)*1000, results.getLong(8)*1000, results.getInt(9), 
						results.getInt(10), results.getInt(11), results.getDouble(12),
						results.getDouble(13), results.getDouble(15), results.getDouble(14));
				processedResults.add(d);
			}
			
			DatabaseInterface.returnThread(processedResults);
		} catch (SQLException e) {
			e.printStackTrace();
			DatabaseInterface.returnThread(null);
		}
	}

	/**
	 * Creates a new DatabaseHandle object.
	 * @param connection
	 * A connection to the database this object will interact with.
	 * @param query
	 * The query to execute.
	 * @author D�rio Tavares Antunes
	 */
	public DatabaseHandle(Connection connection, String query) {
		handle = connection;
		this.query = query;
	}

}
