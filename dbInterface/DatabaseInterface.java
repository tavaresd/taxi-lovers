package dbInterface;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import taxiData.DataContainer;

/**
 * DatabaseInterface.java
 * @author D�rio Tavares Antunes
 * An interface to the underlying SQLite databases.
 */
public class DatabaseInterface {

	private static Connection[] multiConnect;
	private static int counter = 0;
	private static ArrayList<DataContainer> returnData;
	private static Lock queryLock = new ReentrantLock();
	private static Lock additionLock = new ReentrantLock();
	private static Condition returnReady = additionLock.newCondition();

	/**
	 * method initialise 
	 * Creates connections to a number of different databases, each containing
	 * part of the total dataset, allowing multithreaded querying.
	 * @param databasesDir
	 * The directory containing the database files.
	 * @author D�rio Tavares Antunes
	 */
	public static void initialise(String databasesDir) {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {}
		
		// Finds all the database files in the directory given and connects to 
		// them.
		File databaseDirectory = new File(databasesDir);
		String[] names = databaseDirectory.list();
		int dbFiles = 0;
		for (String s : names) {
			if (s.endsWith(".db")) {
				dbFiles++;
			}
		}
		multiConnect = new Connection[dbFiles];

		int index = 0;
		for (int i = 0; i < names.length; i++) {
			if (names[i].endsWith(".db")) {
				try {
					multiConnect[index++] = DriverManager.getConnection("jdbc:sqlite:" + databaseDirectory + "/" + names[i]);
				} catch (SQLException e) {}
			}
		}
	}

	/**
	 * method query
	 * Uses a number of databases, each handled in its own thread, to accelerate
	 * fetching data. It then stiches the results into a single ArrayList.
	 * @param query
	 * A valid SQL query containing specification to search by.
	 * @return
	 * An ArrayList of DataContainers, the results of the query.
	 * @author D�rio Tavares Antunes
	 */
	public static ArrayList<DataContainer> query(String query) {
		// Uses a lock to prevent multiple bits of code trying to access the
		// database at once, as SQLite only allows one connection per file.
		queryLock.lock();
		returnData = new ArrayList<>();

		for (Connection c : multiConnect) {
			Thread t = new Thread(new DatabaseHandle(c, query));
			t.start();
		}
		
		// Waits for every database to return results before returning the full set.
		additionLock.lock();
		try {
			returnReady.await();
		} catch (InterruptedException e) {
			//this doesn't happen
		}
		additionLock.unlock();
		
		queryLock.unlock();
		
		return returnData;
	}

	/**
	 * method returnThread
	 * Implements a counting semaphore and adds found data to the final ArrayList
	 * to be returned. Once all threads return, it signals that the query has
	 * finished and can be returned to the caller.
	 * @param data
	 * The data found by one of the threads.
	 * @author D�rio Tavares Antunes
	 */
	public static void returnThread(ArrayList<DataContainer> data) {
		// Waits for other threads to finish appending their results to the
		// result set before adding its own to avoid race conditions.
		additionLock.lock();
		
		returnData.addAll(data);
		
		// Signals that the query can return if all the threads have finished.
		if (++counter == multiConnect.length) {
			counter = 0;
			returnReady.signal();
		}
		
		additionLock.unlock();
	}

}
