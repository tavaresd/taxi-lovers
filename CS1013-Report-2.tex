\documentclass[]{article}
\usepackage[margin=1.3in]{geometry}

\title{CS1013 Programming Project Report, Group 2}
\author{
	Fitzpatrick, Joseph
	\and 
	O'Boyle, Patrick
	\and
	O' Carroll, Se\'{a}n 
	\and
	Ormsby, Dan 
	\and
	Tavares Antunes, D\'{a}rio}


\begin{document}
\maketitle

\tableofcontents

\section{Design Outline}

The project was designed for maximal modularity in order to make it easy to add and extend features as we went through design stages.

As such, we identified three main sections of the project - data, data processing and visualisation. Each would be dealt with separately and interact with the others through a simple interface. However, this interaction is limited to sections directly adjacent to them. That is, only the data processing section interacts with both of the others.

This prevents problems in one section propagating across the project in unpredictable ways, making them significantly easier to track down.

\section{Team Organisation}

The team used a primarily flat management structure, with everyone contributing equally to project planning and delegation, generally resulting in people contributing to the project in sections they wanted to contribute to. When delegation or more long-term planning was needed, D\'{a}rio took the team role, assigning issues and making long-term design decisions.

Team organisation was achieved through a mixture of online messaging in a group chat on Facebook, assignment of issues through Gitlab's web interface and meetings in person when large amounts of discussion or organisation needed to be done. In the odd case where a member could not attend the meeting in person, it was possible to arrange to have them on video conference so they could still contribute.

This worked very well in general, with all team members satisfied with their work and the organisation of the project.

\section{Features Implemented}

Features were implemented in the 3 main sections of the project and can be discussed separately.

\subsection{Data}

\subsubsection{Conversion to Database, D\'{a}rio Tavares Antunes}

The data (a reduced subsection of it, discussed below in Problems Encountered) was kept in SQLite databases. There are multiple databases in order to allow concurrency to be used in order to accelerate querying the entire dataset and parsing it into a form usable in the rest of the project.

SQLite allows only one connection per database. Having multiple databases allowed the querying code to have a thread per database which would query it, parse the results and pass them back to a main thread, which would then return it to the calling code.

In terms of actual implementation, the interface exposed a blocking method, \\\texttt{query(SQL Statement)}, which would query all the databases, parse them into usable Java objects and only then return them all in a dynamic array.

This involved the use of mutexes and counting semaphores. These are outside the scope of the report but are suitably commented in the code.

\subsubsection{Retrieval from Database, Patrick O'Boyle}

The second part of the handling data involved generating valid SQL queries from different kinds of input. Patrick implemented methods that would generate valid SQL statements for varying inputs, such as user selection in checkboxes (section 3.2.2) or String arrays of conditions written directly into code (hard-coded).

\subsection{Data Processing}

Each member of the project made their own predefined query, which demonstrated an ability to interact with the data and visualisation sections of the project.

\subsubsection{Speed/Time Graph, Joseph Fitzpatrick}

Joseph produced a graph which plots taxis by their passenger count, average speed, ratecode and time taken.

A set of concentric circles were drawn on top of each other, each one signalling a one minute increase in trip time. For each trip, a line was drawn from a centre circle outwards, with the length indicating the time taken, the size of a circle at the end of the line signifying the passenger count and the colour of the line indicating the average speed, from green (fastest) to orange.

The ratecode determines the colour of the circle at the end of the line, but as 98.33\% of our sample has rate code 1, seeing any difference is difficult.

\subsubsection{User Queries, Patrick O'Boyle}

Patrick created a user interface which allowed users to select from certain fields with basic true/false selections (i.e. check-boxes), for data points to plot onto a map. This allows the user to select from certain rate codes and vendors, seeing their results plotted on a map.

User queries can vary in what the returned results can signify. As such, the trips returned from the query were simply plotted with the pick-up and drop-off locations being connected by a simple line.

To avoid the map becoming over-crowded and nothing being visible in any reasonable way, a maximum number of points that could be plotted was defined (set to 1000 in the version delivered) so that queries would not blot out the map nor plot a small amount of points which show nothing.

\subsubsection{Heat Maps, Se\'{a}n O' Carroll}

Se\'{a}n worked on producing a heat map of taxi pick-up locations in and around Manhattan Island.

This was done by dividing the map into individual heat zones using the SimplePolygonMarker class from the UnfoldingMaps library. The number of pick-ups within each zone was tallied up to determine what colour an individual zone should be, ranging from green to red.

\subsubsection{Airport Journeys, Dan Ormsby}

Dan worked on a visualisation of the number of taxis travelling from Manhattan to JFK Airport, chosen for its stand out location and distance from the most active areas.

A $5\times5$ grid was superimposed onto Manhattan, then lines drawn from the centre of each cell to JFK. The width of these lines was determined by the number of taxis in that grid, with each line being assigned a random colour to easily differentiate between each cell and made slightly transparent so that clarity is preserved even when lines overlap.

\subsubsection{Popular Location Finder, D\'{a}rio Tavares Antunes}

D\'{a}rio worked on a query that detects clusters of taxis, with what constitutes a cluster being defined by the user (multiple times, if they so wish). Taxis are defined as being in a cluster if they are no more than a certain distance from any other taxi in their group. To generate a group, a random taxi is first selected.

A button on the screen for this query allows the user to define the distance in which a taxi is considered to be within a cluster.

Once a cluster of taxis is found, an outline is drawn around them using a convex hull algorithm.

\subsection{Visualisation/GUI}

The user interface underwent modification by every single member of the project throughout the design stages, with a barebones implementation similar to that created in labs implemented at first by Dan Ormsby which was iterated on as the project progressed by each member in order to suit their needs.

Patrick implemented generation of new screens in order to allow multiple user queries to be run, Dário added a limitation on buttons that allowed them to be no smaller than the width required to display their label with a font no less than one third of the height of the button, as well as making them more aesthetically pleasing.

\section{Problems Encountered}

\subsection{Data Querying}

The main problem encountered was one of the magnitude of the dataset used. The original dataset (28 million entries) was too slow and cumbersome to search through. As statistically significant samples can be much, much smaller than this set and any differences in visualisation are barely noticeable once over 10,000 data points, the set was reduced to roughly 500,000 entries by only keeping every 57th entry.

However, the database software settled on for the project was SQLite, which only allows one open connection per file. As such, the database was instead split into 10 files (although there is an infrastructure in place that can support any number of database files), which are then all accessed concurrently, as discussed above.

\subsection{Flawed Dataset}

The original dataset was found to contain invalid trips, having 0 passengers, or travelling 0.0km, or having a trip time of 0 seconds, or having no drop-off location or pick-up location, or in some cases having multiple problems of the same sort.

To avoid this affecting visualisations in the final program, invalid entries were simply ignored when the database was being generated.

\subsection{Messy Drawing Code}

The Screen class dealt with drawing and interacting with individual objects, as well as widgets assigned to it. However, this let to spaghetti code and generally messy if-else statements. To avoid this and to make the project easier to extend in future, a Java \texttt{interface} class was created, one for objects that implement their own drawing method and another for one that implements methods that handle mouse clicking and other such interactions.

This led to vastly simplified code, which checked whether that particular instance of the Screen class contained an object which implemented one of those interface and simply called them when possible. This avoided having a string of if-else statements for each possible case.

\subsection{Magic Numbers}

Throughout the code, magic numbers were scattered, which cause innumerable problems such as repeated event codes and code which didn't adapt to changed conditions elsewhere in the code. This led to the creation of a class containing a number of constants and another for event codes, which vastly simplified the code.

\end{document}          
