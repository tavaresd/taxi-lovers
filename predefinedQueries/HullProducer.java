package predefinedQueries;

import guiElements.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import mapInterface.TextMarker;
import processing.core.PApplet;
import taxiData.*;
import dbInterface.DatabaseInterface;
import dbInterface.Utility;
import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.marker.*;
import de.fhpotsdam.unfolding.providers.Google;

/**
 * HullProducer.java
 * @author D�rio Tavares Antunes
 * Finds all taxi pickup locations within a given range of any other pickup,
 * grouping them and then drawing a convex hull around the groups.
 */
public class HullProducer implements Interactable, Drawable {

	private static final ArrayList<DataContainer> RAW_DATA;
	private static final double GRID_LONGEST;
	private static final int GRID_SIDE;
	private int checkRadius = 1;
	public UnfoldingMap map;
	private static PApplet parent = TaxiLovers.mainApplet;
	private Widget mark;
	private boolean beginMark = false;
	private float startX = -1, startY = -1;
	private boolean resumeReady = true;
	private int frame = 1, increment = 1;

	static {
		RAW_DATA = DatabaseInterface.query(Utility.generateQuery(new String[] {"ID < 10000"}));

		// Finds the longest side of a grid for use later
		GRID_SIDE = (int) (Math.sqrt(RAW_DATA.size()));
		GRID_LONGEST = 0.38d / GRID_SIDE;
	}

	/**
	 * Creates a HullProducer, which shows an Unfolding map to allow the user
	 * to define the range for group generation.
	 * @author D�rio Tavares Antunes
	 * 
	 */
	public HullProducer() {
		map = new UnfoldingMap(parent, new Google.GoogleMapProvider());
		Location nyLocation = new Location(40.74, -73.99);

		// Show map around the location in the given zoom level.
		map.zoomAndPanTo(13, nyLocation);

		float maxPanningDistance = 40; // in km
		map.setPanningRestriction(nyLocation, maxPanningDistance);
		map.setZoomRange(C.MAX_ZOOM, C.MIN_ZOOM);

		mark = new Widget(parent.width - 72, 
				20, 0, C.BUTTON_HEIGHT, "Set", parent.color(205, 0, 205),
				parent.color(255, 0, 255), Events.MARK);
	}

	@Override
	/**
	 * method draw
	 * Draws the current state of the map, the set button and a line linking
	 * the first point selected to the mouse to simplify selecting a reasonable
	 * distance.
	 * @author D�rio Tavares Antunes
	 */
	public void draw() {
		map.draw();
		mark.draw();
		parent.fill(0);

		// Draws a line from the first click to the current mouse position when
		// setting the distance to filter taxis out by.
		if (startX != -1) {
			parent.stroke(1);
			parent.line(startX, startY, parent.mouseX, parent.mouseY);
		}

		// Writes a loading message at the mouse position while the query runs.
		if (!resumeReady) {
			if (frame == 119 || frame == 0) {
				increment = -increment;
			}
			frame += increment;
			StringBuilder out = new StringBuilder("Working");
			for (int i = 0; i < (frame/40) + 1; i++)
				out.append(".");
			parent.text(out.toString(), parent.mouseX, parent.mouseY);
		}
	}

	//These methods are here because the Interactable interface requires them
	@Override
	public void mouseMoved() {} // not used

	@Override
	public int getEvent(int mouseX, int mouseY) {
		return Events.INVALID; // not used
	}

	/**
	 * method mousePressed
	 * Called when the user clicks the mouse. If the last query has completed,
	 * it allows the user to start a new query by clicking the set button, or
	 * to finish defining the range if they've already started.
	 * @author D�rio Tavares Antunes
	 */
	public void mousePressed() {
		// If there is no query set, starts or finishes setting the distance to
		// filter taxis by.
		if (resumeReady) {
			if (mark.getEvent(parent.mouseX, parent.mouseY) == Events.MARK && !beginMark) {
				beginMark = true;
			} else if (beginMark) {
				if (startX != -1) {
					Location last = map.getLocation(startX, startY);
					Location current = map.getLocation(parent.mouseX, parent.mouseY);
					double dLat = last.getLat() - current.getLat();
					double dLon = last.getLon() - current.getLon();
					double distance = dLat * dLat + dLon * dLon;
					checkRadius = (int)(Math.sqrt(distance)/GRID_LONGEST);
					if (checkRadius < 1)
						checkRadius = 1;
					islandOutline(dLat * dLat + dLon * dLon);
					startX = startY = -1;
					beginMark = false;
				} else {
					startX = parent.mouseX;
					startY = parent.mouseY;
				}
			}
		}
	}

	/**
	 * method islandOutline
	 * Clears the map of any markers it currently has and begins a thread that
	 * will draw new groups and markers onto the map as it progresses.
	 * @param distance
	 * The square of the distance a taxi can be from another taxi in its group.
	 * Using the square allows the distance comparison to not use costly square
	 * root functions.
	 * @author D�rio Tavares Antunes
	 */
	public void islandOutline(double distance) {
		resumeReady = false;

		// Removes any markers from previous queries. Looping has to be disabled
		// or else concurrency exceptions occur.
		parent.noLoop();
		try { Thread.sleep(50); } catch (Exception e) {}
		try {
			for (int i = 0;;)
				map.removeMarkerManager(i);
		} catch (Exception e) {}
		parent.loop();

		// Starts a thread that generates groups and convex hulls
		HullProcessor h = this.new HullProcessor(map, distance);
		Thread th = new Thread(h);
		th.start();
	}

	/**
	 * method groupCandidate
	 * Finds a group of taxis.
	 * @param data
	 * A two dimensional matrix containing all the taxis, split up by location,
	 * with each element holding an ArrayList of all the taxis at that location.
	 * @param distance
	 * The square of the distance a taxi may be from another taxi in its group.
	 * @param check
	 * The number of grid squares away from any given grid square a taxi can be
	 * before it will always be out of range.
	 * @return
	 * An array of CoordinatePairs containing the pickup locations of taxis in 
	 * a group.
	 * @author D�rio Tavares Antunes
	 */
	public static CoordinatePair[] groupCandidate(ArrayList<DataContainer>[][] data, double distance, int check) {
		ArrayList<Candidate> candidates = new ArrayList<>();
		ArrayList<DataContainer> cell = null;

		// Finds a cell to seed the popular groups from.
		int x = data.length - 1, y = 0;
		for (; x > -1 && cell == null; x--) {
			for (y = data.length - 1; y > -1 && cell == null; y--) {
				if (data[y][x].size() > 0 ) {
					cell = data[y][x];
				}
			}
		}

		// This means all cells are empty and the query has completed.
		if (cell == null)
			return null;

		// The seed point is the first taxi in the given cell, from which
		// distances to other taxis in a reasonable range are found.
		candidates.add(new Candidate(cell.get(0), x, y));
		cell.remove(0);
		ArrayList<CoordinatePair> done = new ArrayList<>();

		/* This while loop iterates through the candidates list until there are
		 * none left.
		 * The distance from a given candidate to any other taxi in a range of 
		 * cells around its cell is found. If the distance falls within the user
		 * defined range, the taxi is added to the candidate list.
		 * Once the current candidate has been checked against all valid taxis
		 * to compare against, it is removed from the candidate list.
		 */
		while (candidates.size() > 0) {
			for (int i = candidates.size() - 1; i != -1; i--) {
				Candidate current = candidates.get(i);
				double lat = current.data.getPickupCoordinates().getLatitude();
				double lon = current.data.getPickupCoordinates().getLongitude();
				for (int j = current.x - check; j <= current.x + check; j++) {
					for (int k = current.y - check; k <= current.y + check; k++) {
						try {
							ArrayList<DataContainer> gridCell = data[k][j];
							for (int l = gridCell.size() - 1; l > -1; l--) {
								if (!gridCell.get(l).equals(current.data)) {
									CoordinatePair o = gridCell.get(l).getPickupCoordinates();
									double dy = lat - o.getLatitude();
									double dx = lon - o.getLongitude();
									if (dy*dy + dx*dx < distance) {
										candidates.add(new Candidate(gridCell.get(l), j, k));
										gridCell.remove(l);
									}
								}
							}
						} catch (ArrayIndexOutOfBoundsException e) {}
					}
				}

				done.add(current.data.getPickupCoordinates());
				candidates.remove(i);
			}
		}

		// The points in a group are copied into an array for use in the convex
		// hull generating algorithm.
		CoordinatePair[] mass = new CoordinatePair[done.size()];
		for (int i = 0; i < mass.length; i++) 
			mass[i] = done.get(i);

		return mass;
	}

	/**
	 * method cross
	 * Returns the directional area of the triangle formed by three given 
	 * points. The sign can be used to determine whether the line from one
	 * point to another turns in towards or away from the other points. As
	 * all points on the convex hull turn outwards from the other points,
	 * this can be used to determine the hull quickly.
	 * @author Wikipedia Contributor
	 */
	private static long cross(CoordinatePair O, CoordinatePair A, CoordinatePair B) {
		return (long) (((A.getLongitude() - O.getLongitude()) * (B.getLatitude() - O.getLatitude()) - 
				(A.getLatitude() - O.getLatitude()) * (B.getLongitude() - O.getLongitude())) * 1000000);
	}

	/**
	 * method convexHull 
	 * @param points
	 * The set of points to draw a convex hull around.
	 * @return
	 * The points on the convex hull.
	 * @author Wikipedia Contributor
	 */
	private static CoordinatePair[] convexHull(CoordinatePair[] points) {
		if (points.length > 1) {
			int n = points.length;
			int k = 0;
			CoordinatePair[] hull = new CoordinatePair[n << 1];
			Arrays.sort(points);

			// Build lower hull
			for (int i = 0; i < n; ++i) {
				while (k >= 2 && cross(hull[k - 2], hull[k - 1], points[i]) <= 0)
					k--;
				hull[k++] = points[i];
			}

			// Build upper hull
			for (int i = n - 2, t = k + 1; i >= 0; i--) {
				while (k >= t && cross(hull[k - 2], hull[k - 1], points[i]) <= 0)
					k--;
				hull[k++] = points[i];
			}

			if (k > 1) {
				// remove non-hull vertices after k; remove k - 1 which is a duplicate
				hull = Arrays.copyOfRange(hull, 0, k - 1);
			}
			return hull;
		} else if (points.length <= 1) {
			return points;
		} else {
			return null;
		}
	}

	/**
	 * Internal class HullProcessor
	 * @author D�rio Tavares Antunes
	 * Generates the groups and convex hulls asynchronously, adding them to the
	 * map as it goes.
	 */
	class HullProcessor implements Runnable {

		UnfoldingMap map;
		double distance;

		public HullProcessor(UnfoldingMap maps, double distance) {
			map = maps;
			this.distance = distance;
		}

		@Override
		/**
		 * method run
		 * Called when this object's thread is started.
		 * Generates the groups, generates the convex hulls around them and
		 * adds them to the map as it goes.
		 * @author D�rio Tavares Antunes
		 */
		public void run() {
			float dataSize = RAW_DATA.size();

			// A grid is generated to be no larger than strictly necessary.
			int gridSize = (int) (Math.sqrt(RAW_DATA.size()));

			double maxLon = -90;
			double minLon = 90;
			double maxLat = -180;
			double minLat = 180;

			for (DataContainer c : RAW_DATA) {
				CoordinatePair coords = c.getPickupCoordinates();
				if (coords.getLongitude() > maxLon) {
					maxLon = coords.getLongitude();
				} else if (coords.getLongitude() < minLon) {
					minLon = coords.getLongitude();
				}
				if (coords.getLatitude() > maxLat) {
					maxLat = coords.getLatitude();
				} else if (coords.getLatitude() < minLat) {
					minLat = coords.getLatitude();
				}
			}

			double latStep = (maxLat - minLat)/(gridSize - 1);
			double lonStep = (maxLon - minLon)/(gridSize - 1);

			@SuppressWarnings("unchecked")
			ArrayList<DataContainer>[][] grid = new ArrayList[gridSize][gridSize];
			for (int i = 0; i < grid.length; i++) {
				for (int j = 0; j < grid[0].length; j++) {
					grid[i][j] = new ArrayList<DataContainer>();
				}
			}

			// Puts each taxi into its relevant grid square
			for (DataContainer d : RAW_DATA) {
				CoordinatePair c = d.getPickupCoordinates();
				int y = (int) ((c.getLatitude() - minLat)/latStep);
				int x = (int) ((c.getLongitude() - minLon)/lonStep);
				grid[y][x].add(d);
			}

			// While there's still groups to be found, draws outlines around
			// them and plots some of the points in them.
			CoordinatePair[] a;
			while ((a = groupCandidate(grid, distance, checkRadius)) != null) {
				float aSize = a.length;

				CoordinatePair[] hull = convexHull(a);

				if (hull.length > 3) {
					double ymax = -90;
					double ymin = 90;
					double xmin = 180;
					double xmax = -180;
					for (CoordinatePair c : hull) {
						if (c.getLongitude() > xmax)
							xmax = c.getLongitude();
						if (c.getLongitude() < xmin)
							xmin = c.getLongitude();
						if (c.getLatitude() > ymax)
							ymax = c.getLatitude();
						if (c.getLatitude() < ymin)
							ymin = c.getLatitude();
					}
					double x = (xmin + xmax)/2;
					double y = (ymin + ymax)/2;
					Location tmp = new Location(y, x);
					DecimalFormat df = new DecimalFormat("##.##%");
					DecimalFormat cf = new DecimalFormat("###,###");
					MarkerManager<Marker> tmpM = new MarkerManager<Marker>();
					String text = df.format(aSize/dataSize) + (aSize/dataSize > 0.004 ?  "\n" + cf.format(aSize) : "");
					int inc = a.length/500;
					if (inc == 0)
						inc = 20;
					for (int i = 0; i < hull.length; i++) {
						Location l = new Location(hull[i].getLatitude(), hull[i].getLongitude());
						int index = (i+1) == hull.length ? 0 : i+1;
						Location l2 = new Location(hull[index].getLatitude(), hull[index].getLongitude());
						SimpleLinesMarker s = new SimpleLinesMarker(l, l2);
						s.setStrokeWeight(1);
						s.setColor(0);
						s.setStrokeColor(0);
						tmpM.addMarker(s);
					}
					int colour = parent.color(parent.random(255), parent.random(255), parent.random(255));
					for (int i = 0; i < a.length; i+= inc) {
						CoordinatePair c = a[i];
						SimplePointMarker s = new SimplePointMarker(new Location(c.getLatitude(), c.getLongitude()));
						s.setColor(colour);
						tmpM.addMarker(s);
					}
					tmpM.addMarker(new TextMarker(tmp, text, parent.color(100, 100, 255)));
					parent.noLoop();
					try {
						Thread.sleep(200);
					} catch (Exception e) {}
					map.addMarkerManager(tmpM);
					parent.loop();
				}
			}
			resumeReady = true;
		}

	}

}