package predefinedQueries;

import dbInterface.Utility;
import java.util.ArrayList;
import processing.core.PApplet;
import taxiData.DataContainer;
import taxiData.TaxiLovers;
import dbInterface.DatabaseInterface;
import guiElements.Drawable;

public class Graph implements Drawable {

	/*
	 * 
	 * Draws graphical representation of taxi data 
	 * Allows the user to easily read off the trip time of each taxi and its speed, number of passengers and rate code
	 * @author Joseph Fitzpatrick
	 * 
	 */
	int check = 1;
	private static PApplet parent = TaxiLovers.mainApplet;
	ArrayList<DataContainer> data;
	ArrayList<DataContainer> data2;
	boolean graph =false;
	int i = 10;
	int pointsChecked = 0;
	int numPas =1;
	long lastDraw = 0;

	@Override
	public void draw(){
		
		if (parent.millis() - lastDraw > 1000) {
			drawBackground();
		}

		if (parent.millis() - lastDraw > 100){
			parent.pushMatrix();
			drawGraph(i);
			i+=10;
			parent.popMatrix();
			lastDraw = parent.millis();
		}

	}

	public void drawGraph(int count){


		data = DatabaseInterface.query(Utility.generateQuery(new String[] { "ID > "+i, "ID < "+(i+10)}));

		int  unitCircle = (parent.width/6)-40;
		parent.noStroke();
		parent.translate(parent.width/2, parent.height/2);

		/*
		 * Draws circles which indicate the time of each trip
		 * Each circle indicates a minute in time travel
		 *  
		 */


			if(i%850 ==0 || i< 11 || parent.mousePressed==true){
				drawBackground();
			}

		
		int dataSetSize = data.size();
		/*
		 * Draws a line from the outer radius of the inner circle to the point which corresponds to the trip length 
		 * The color of the line corresponds to the speed of the taxi, green being fast and red being slow
		 * The size of the point at the end of each line is the number of passengers per taxi
		 * The color of this point reflects the rate code
		 */
		for (int i = 0; i < dataSetSize-1; i++){

			int speedValue = (int) (((data.get(i).getTripDistance()*100)/(data.get(i).getTripLength()))*100);
			int speed = parent.color((speedValue*60)%255,  100, 70);

			parent.stroke(speed, 255);

			float xOnCirlce = (float) ((unitCircle)*Math.cos((pointsChecked + i/10f)*2*Math.PI/dataSetSize) );
			float yOnCircle = (float) ((unitCircle)*Math.sin((pointsChecked + i/10f)*2*Math.PI/dataSetSize));

			float x2OffCircle = (float) ((unitCircle + data.get(i).getTripLength()/20)*Math.cos(
					(pointsChecked + i/10f)*2*Math.PI/(dataSetSize)));

			float y2OffCircle = (float) ((unitCircle + data.get(i).getTripLength()/20)*Math.sin(
					(pointsChecked + i/10f)*2*Math.PI/(dataSetSize)));

			parent.line(xOnCirlce, yOnCircle, x2OffCircle, y2OffCircle);

			parent.pushStyle();
			parent.stroke(data.get(i).getRateCode(), 50);
			parent.strokeWeight(data.get(i).getPassengerCount()+1);
			parent.point(x2OffCircle, y2OffCircle);
			parent.popStyle();

		}
		pointsChecked += dataSetSize-1;

	}
	
	public void drawBackground() {
		parent.background(0);
		parent.pushStyle();

		for(int j = parent.width; j>90 ; j-=40){
			parent.fill((int)(Math.random()*510)%255,(int)(Math.random()*660)%255,(int)(Math.random()*790)%255, 50);
			parent.ellipse(0, 0, j, j);
		}

		parent.popStyle();
	}



}

