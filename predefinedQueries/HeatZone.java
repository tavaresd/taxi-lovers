package predefinedQueries;

import java.util.ArrayList;

import processing.core.PApplet;
import taxiData.TaxiLovers;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.marker.SimplePolygonMarker;

/**
 * HeatZone.java
 * extends SimplePolygonMarker
 * @author Sean O'Carroll
 * Square with color based on number of taxis in the area
 */
public class HeatZone extends SimplePolygonMarker {
	
	private int noOfTaxis;
	private static PApplet parent = TaxiLovers.mainApplet;
	
	public HeatZone(ArrayList<Location> corners, int noOfTaxis) {
		super(corners);
		
		this.noOfTaxis = noOfTaxis;
		this.color = parent.color(0,0,0,0);
		this.strokeWeight = 0;
		this.strokeColor = parent.color(0, 0, 0, 0);
	}
	
	/**
	 * method setColor
	 * Sets the color of the zone to a colour between green and red based on
	 * a color value between 0 and 100
	 * @author Sean O'Carroll
	 */
	public void setColor(int colValue) {
		int r = (int)((255 * colValue) / 100);
		int g = (int)((255 * (100 - colValue)) / 100 );
		this.color = parent.color(r, g, 0, 200);
		this.strokeWeight = 0;
		this.strokeColor = parent.color(r, g, 0, 200);
	}
	
	public int getNoOfTaxis() {
		return noOfTaxis;
	}
}
