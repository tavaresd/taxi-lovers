package predefinedQueries;

import guiElements.Drawable;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import processing.core.PApplet;
import taxiData.C;
import taxiData.DataContainer;
import taxiData.TaxiLovers;
import dbInterface.DatabaseInterface;
import dbInterface.Utility;
import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.providers.Google;

/**
 * HeatMap.java
 * @author Sean O'Carroll
 * Grid of HeatZone squares to represent a heat map.
 */
public class HeatMap implements Drawable {

	private ArrayList<HeatZone> heatMarkers;

	private final double DIVISIONS = 120.0;
	private static ArrayList<DataContainer> data;
	public UnfoldingMap map;
	private static final Location NY_LOCATION = new Location(40.74, -73.99);
	/*
	private static final Location TOP_LEFT = new Location(40.81007, -73.9657);
	private static final Location TOP_RIGHT = new Location(40.81007, -73.92374);
	private static final Location BOTTOM_LEFT = new Location(40.71113, -74.025536);
	private static final Location BOTTOM_RIGHT= new Location(40.71113, -74.025536);
	*/
	
	private static final Location TOP_LEFT = new Location(40.803963, -74.010254);
	private static final Location BOTTOM_RIGHT = new Location(40.713207, -73.94571);
	private static PApplet parent = TaxiLovers.mainApplet;

	static {
		data = DatabaseInterface.query(Utility.generateQuery(new String[]{"ID < 750"}));
	}

	/**
	 * method draw
	 * Called periodically to update screen
	 * @author Sean O'Carroll
	 */
	public void draw() {
		parent.background(100);
		map.draw();
		Location loc = map.getLocation(parent.mouseX, parent.mouseY);
		parent.fill(0);
		parent.text(loc.getLat() + ", " + loc.getLon(), parent.mouseX, parent.mouseY);
	}


	public HeatMap() {

		map = new UnfoldingMap(parent, new Google.GoogleTerrainProvider());

		// Show map around the location in the given zoom level.
		map.zoomAndPanTo(13, NY_LOCATION);

		float maxPanningDistance = 40; // in km
		map.setPanningRestriction(NY_LOCATION, maxPanningDistance);
		map.setZoomRange(C.MAX_ZOOM, C.MIN_ZOOM);


		heatMarkers = new ArrayList<>();


		double lonIncrement = round(Math.abs(TOP_LEFT.getLon() - BOTTOM_RIGHT.getLon())/DIVISIONS, 5);
		double latIncrement = round(Math.abs(TOP_LEFT.getLat() - BOTTOM_RIGHT.getLat())/DIVISIONS, 5);

		int max = 0;
		int min = 0;

		double lonEnd = round(BOTTOM_RIGHT.getLon(), 5);
		double latEnd = round(BOTTOM_RIGHT.getLat(), 5);

		for (double lon=TOP_LEFT.getLon(); lon <= lonEnd; lon += lonIncrement) {
			for (double lat=TOP_LEFT.getLat(); lat >= latEnd; lat-= latIncrement) {

				Location l0 = new Location(lat, lon);
				Location l1 = new Location(lat, lon+lonIncrement);
				Location l2 = new Location(lat+latIncrement, lon+lonIncrement);
				Location l3 = new Location(lat+latIncrement, lon);

				ArrayList<Location> corns = new ArrayList<Location>();
				corns.add(l0);
				corns.add(l1);
				corns.add(l2);
				corns.add(l3);

				int taxisWithin = 0;

				for (DataContainer taxi : data) {
					if (taxi.getPickupCoordinates().getLatitude() <= lat && 
							taxi.getPickupCoordinates().getLatitude() >= lat-latIncrement &&
							taxi.getPickupCoordinates().getLongitude() >= lon &&
							taxi.getPickupCoordinates().getLongitude() <= lon+lonIncrement) {
						taxisWithin++;
					}
				}

				if(taxisWithin > max) {
					max = taxisWithin;
				} else if (taxisWithin < min) {
					min = taxisWithin;
				}

				HeatZone z0 = new HeatZone(corns, taxisWithin);
				heatMarkers.add(z0);
			}
		}

		double range = (double)(max - min);

		//Calculate a weighted percentage for each heat zone
		for (HeatZone h : heatMarkers) {
			double weight = (double)h.getNoOfTaxis()/range;
			if (weight < 0.5) {
				weight += 0.2;
			}
			int percentage = (int)(weight*100.0);
			h.setColor(percentage);
		}


	}

	/**
	 * method addHeatZones
	 * Adds all the heat zones in the heat map to a map
	 * @author Sean O'Carroll
	 */
	public void addHeatZones(UnfoldingMap map0) {
		for (HeatZone m: heatMarkers) {
			map0.addMarker(m);
		}
	}

	/**
	 * method round
	 * Rounds a double to n decimal places
	 * @author Sean O'Carroll
	 */
	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
}