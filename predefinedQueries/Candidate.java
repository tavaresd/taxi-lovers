package predefinedQueries;

import taxiData.DataContainer;

/**
 * Candidate.java
 * @author D�rio Tavares Antunes
 * A data holder for the HullProducer class.
 */
public class Candidate {
	
	DataContainer data;
	int x;
	int y;
	
	public Candidate(DataContainer d, int x, int y) {
		data = d;
		this.x = x;
		this.y = y;
	}

}
