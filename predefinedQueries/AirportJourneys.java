package predefinedQueries;

import java.util.ArrayList;

import mapInterface.TaxiLineMarker;
import mapInterface.TaxiMapInterface;
import taxiData.C;
import taxiData.DataContainer;
import taxiData.TaxiLovers;
import dbInterface.DatabaseInterface;
import dbInterface.Utility;
import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.providers.Google;

//TODO: Break lines at reasonable lengths
public class AirportJourneys {

	/**
	 * @author Dan Ormsby
	 * Divides Manhattan into a grid. A single line will show the amount of journeys to
	 * the airport from that square. Line width will increase the greater the # of taxis.
	 */
	public static UnfoldingMap airportJourneys() {
		final double TOP_LO = 40.82;
		final double BOT_LO = 40.698944;
		final double LO_DIFF = TOP_LO - BOT_LO;
		final double LEFT_LA = -74.02;
		final double RIGHT_LA = -73.941;
		final double LA_DIFF = RIGHT_LA - LEFT_LA;
		UnfoldingMap map = new UnfoldingMap(TaxiLovers.mainApplet, new Google.GoogleTerrainProvider());
		ArrayList<DataContainer> data = null;
		int strokeWeight = 0;
		int[][] numberOfTaxis = new int[5][5];
		for(int i = 0; i < 5; i++){
			for(int j = 0; j < 5; j++){
				data = DatabaseInterface.query(Utility.generateQuery(new String[]{"PICKUPLO < " + (TOP_LO - ((LO_DIFF/5)*i)), "PICKUPLO > " + (TOP_LO - ((LO_DIFF/5)*(i+1))),"PICKUPLA > " + (LEFT_LA + ((LA_DIFF/5)*j)), "PICKUPLA < " + (LEFT_LA + ((LA_DIFF/5)*(j+1))),
						"DROPOFFLA > -" + 73.8, "DROPOFFLA < -"+ 73.777, "DROPOFFLO < 40.655"}));
				numberOfTaxis[i][j] = data.size();
			}
		}

		Location nyLocation = new Location(40.74, -73.99);
		Location JFK = new Location(40.6397, -73.7789);
		double x, y;
		//TODO: Change to percentage stroke weight
		for(int i = 0; i < 5; i++){
			for(int j = 0; j < 5; j++){
				if(numberOfTaxis[i][j] >10000)
					strokeWeight = 25;
				else if(numberOfTaxis[i][j] > 500)
					strokeWeight = 20;
				else if(numberOfTaxis[i][j] > 250)
					strokeWeight = 15;
				else if(numberOfTaxis[i][j] > 100)
					strokeWeight = 10;
				else if(numberOfTaxis[i][j] > 10)
					strokeWeight = 5;
				else if(numberOfTaxis[i][j] > 1)
					strokeWeight = 1;

				x = LEFT_LA +((LA_DIFF/5)*j) + (LA_DIFF/10);
				y = TOP_LO - ((LO_DIFF/5)*i) - (LO_DIFF/10);
				TaxiMapInterface.addJourneys(map, JFK, x, y, strokeWeight);
			}
		}
		// Show map around the location in the given zoom level.
		map.zoomAndPanTo(12, nyLocation);

		float maxPanningDistance = 40; // in km
		map.setPanningRestriction(nyLocation, maxPanningDistance);
		map.setZoomRange(C.MAX_ZOOM, C.MIN_ZOOM);

		drawGrid(map);
		return map;
	}

	public static void drawGrid(UnfoldingMap map){
		final double TOP_LO = 40.82;
		final double BOT_LO = 40.698944;
		final double LO_DIFF = TOP_LO - BOT_LO;
		final double LEFT_LA = -74.02;
		final double RIGHT_LA = -73.941;
		final double LA_DIFF = RIGHT_LA - LEFT_LA;
		for(int i = 0; i <= 5; i++){
			TaxiLineMarker m0 = new TaxiLineMarker(LEFT_LA + (LA_DIFF/5)*i, TOP_LO,LEFT_LA + (LA_DIFF/5)*i, BOT_LO);
			map.addMarker(m0);
		}
		for(int i = 0; i <= 5; i++){
			TaxiLineMarker m0 = new TaxiLineMarker(LEFT_LA, TOP_LO - (LO_DIFF/5)*i,RIGHT_LA, TOP_LO - (LO_DIFF/5)*i);
			map.addMarker(m0);
		}
	}

}
