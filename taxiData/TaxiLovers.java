package taxiData;

import guiElements.Checkbox;
import guiElements.Screen;
import guiElements.TextWidget;
import guiElements.Widget;
import guiElements.RadioButton;

import java.util.ArrayList;
import java.util.HashMap;

import mapInterface.TaxiMapInterface;
import predefinedQueries.Graph;
import predefinedQueries.HeatMap;
import predefinedQueries.HullProducer;
import predefinedQueries.AirportJourneys;
import processing.core.PApplet;
import processing.core.PImage;
import dbInterface.DatabaseInterface;
import dbInterface.Utility;
import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.providers.Google;

/**
 * TaxiLovers.java
 * extends PApplet
 * @author Patrick O'Boyle
 * @modified Sean O'Carroll
 * @modified D�rio Tavares Antunes
 * The main class, which runs Processing.
 */
@SuppressWarnings("serial")
public class TaxiLovers extends PApplet {
	ArrayList<DataContainer> data;

	private HashMap<Integer, Screen> screens = new HashMap<>();
	private int currentScreen = 0;
	private int lastScreen = 0;
	private int buttonCount = 0;
	private Widget moveableButton;
	private Widget backButton;

	private final static String DATABASE_DIRECTORY = "databases/";
	private final static String[] BUTTON_LABELS = 
			new String[] {"Graph", "Airport Journeys", "Popular Areas", "Heat Map"};
	public static PApplet mainApplet;

	ArrayList<Widget> widgets = new ArrayList<>();

	/**
	 * method setup
	 * Called once by Processing, sets up the data to be displayed and sets its
	 * location in the applet.
	 * @author D�rio Tavares Antunes
	 * @modified Patrick O'Boyle
	 * @modified Sean O'Carroll
	 * @modified Joseph Fitzpatrick
	 */
	public void setup() {
		mainApplet = this;
		// This opens a connection to each of the database files.
		DatabaseInterface.initialise(DATABASE_DIRECTORY);
		size(600, 600);//, OPENGL); uncomment this if OPENGL works

		backButton = new Widget(20, 20, 0, 
				C.BUTTON_HEIGHT, "Back", C.BUTTON_DEFAULT_COLOUR, 
				C.CORNER_DEFAULT_COLOUR, Events.SCREEN);

		PImage bgImage = loadImage("pataxi.png");
		Screen mainScreen = new Screen(bgImage);

		// Adds a button for each of the pre-defined queries.
		for (int i = 0; i < 4; i++) {
			Widget premadeQuery = new Widget(
					(buttonCount++ % 3) * (C.BUTTON_MARGIN + C.BUTTON_WIDTH) + 20,
					C.FACE_OFFSET + (((buttonCount - 1)/3) * C.BUTTON_MARGIN), 
					C.BUTTON_WIDTH,	C.BUTTON_HEIGHT, BUTTON_LABELS[i], 
					C.BUTTON_DEFAULT_COLOUR, C.CORNER_DEFAULT_COLOUR, 
					Events.SCREEN + (i + 1));
			mainScreen.add(premadeQuery);
		}

		// Adds a button to access Patrick O'Boyle's user query interface.
		moveableButton = new Widget(
				(buttonCount++ % 3) * (C.BUTTON_MARGIN + C.BUTTON_WIDTH) + 20,
				C.FACE_OFFSET + ((buttonCount - 1)/3) * C.BUTTON_MARGIN, C.BUTTON_WIDTH, 
				C.BUTTON_HEIGHT, "New User Query", C.BUTTON_DEFAULT_COLOUR, C.CORNER_DEFAULT_COLOUR,
				Events.NEW_SCREEN);
		mainScreen.add(moveableButton);
		
		TextWidget t = new TextWidget(width/2 + 17, 140, 230, "TAXI LOVERS", color(255, 255, 255));
		mainScreen.add(t);
		t = new TextWidget(width/2 + 17, 195, 153, "Group 2", color(0, 0, 255));
		mainScreen.add(t);
		screens.put(lastScreen++, mainScreen);

        
		// Adds Joseph Fitzpatrick's graph to its own screen.
		Graph graph = new Graph();
		Screen query = new Screen(graph);
		query.add(backButton);
		screens.put(lastScreen++, query);
		
		
		// Adds Dan Ormsby's airport journey visualisation to its own screen.
		UnfoldingMap map = AirportJourneys.airportJourneys();
		Screen query2 = new Screen(map);
		query2.add(backButton);
		screens.put(lastScreen++, query2);

		// Adds D�rio Tavares Antunes' popular zone finding visualisation to
		// its own screen.
		HullProducer h = new HullProducer();
		Screen query3 = new Screen(h, h, h.map);
		query3.add(backButton);
		screens.put(lastScreen++, query3);
		
		// Adds Sean O'Carroll's heat map visualisation to its own screen.
		HeatMap hM = new HeatMap();
		hM.addHeatZones(hM.map);
		Screen presetQuery = new Screen(null, hM, hM.map);
		presetQuery.add(backButton);
		screens.put(lastScreen++, presetQuery);
	}

	public void draw() {
		screens.get(currentScreen).draw();
	}

	/**
	 * method mousePressed
	 * Called when mouse is pressed,
	 * location in the applet.
	 * @author D�rio Tavares Antunes
	 * @modified Patrick O'Boyle
	 * @modified Joseph Fitzpatrick
	 */
	public void mousePressed(){
		screens.get(currentScreen).mousePressed();
		int event = screens.get(currentScreen).getEvent(mouseX, mouseY);

		// If the event demands a screen change, it checks if the screen to
		// change to is valid, then changes to it if so.
		if (event >= Events.SCREEN && event < screens.size()) {
			screens.get(currentScreen).deregister();
			currentScreen = event;
			screens.get(currentScreen).register();
		} else { 
			// If the event does not demand a screen change, enter the event handler.
			switch (event) {
			// Generate a new user query interface screen.
			case Events.NEW_SCREEN:
				Screen mainScreen = screens.get(0);
				mainScreen.remove(moveableButton);
				buttonCount--;
				Widget currentQuery = new Widget(
						(buttonCount++ % 3) * (C.BUTTON_MARGIN + C.BUTTON_WIDTH) + 20,
						C.FACE_OFFSET + ((buttonCount - 1)/3) * C.BUTTON_MARGIN, C.BUTTON_WIDTH,
						C.BUTTON_HEIGHT, "User Query " + (buttonCount - 4), C.BUTTON_DEFAULT_COLOUR, 
						C.CORNER_DEFAULT_COLOUR, Events.SCREEN + lastScreen);
				mainScreen.add(currentQuery);
				if (buttonCount < 15) {
					moveableButton = new Widget(
							(buttonCount++ % 3) * (C.BUTTON_MARGIN + C.BUTTON_WIDTH) + 20,
							C.FACE_OFFSET + ((buttonCount - 1)/3) * C.BUTTON_MARGIN, C.BUTTON_WIDTH, 
							C.BUTTON_HEIGHT, "New User Query", C.BUTTON_DEFAULT_COLOUR, 
							C.CORNER_DEFAULT_COLOUR, Events.NEW_SCREEN);
					mainScreen.add(moveableButton);
				}
				PImage bgImage = loadImage("PaSearchScreen.png");
				Screen s = new Screen(bgImage);

				s.add(backButton);

                s.add(new RadioButton(200, 20, 200, 60, 20, 20, "CMT", "VTS", Events.C1, Events.C2));

                s.add(new Checkbox(350, 20, 20, "Rate Code 0 ", Events.C3));
                s.add(new Checkbox(350, 60, 20, "Rate Code 1", Events.C4));
                s.add(new Checkbox(350, 100, 20, "Rate Code 2", Events.C5));
                s.add(new Checkbox(350, 140, 20, "Rate Code 3", Events.C6));
                s.add(new Checkbox(350, 180, 20, "Rate Code 4", Events.C7));
                s.add(new Checkbox(350, 220, 20, "Rate Code 5", Events.C8));
                s.add(new Checkbox(350, 260, 20, "Rate Code 6", Events.C9));
                s.add(new Checkbox(350, 300, 20, "Rate Code 7", Events.C10));
                s.add(new Checkbox(350, 340, 20, "Rate Code 8", Events.C11));
                s.add(new Checkbox(350, 380, 20, "Rate Code 9", Events.C12));
                s.add(new Checkbox(350, 420, 20, "Rate Code 28", Events.C13));
                s.add(new Checkbox(350, 460, 20, "Rate Code 65", Events.C14));
                s.add(new Checkbox(350, 500, 20, "Rate Code 128", Events.C15));
                s.add(new Checkbox(350, 540, 20, "Rate Code 210", Events.C16));

				widgets = s.getWidgets();

                s.add(new Widget(20, 20 + C.BUTTON_MARGIN, C.BUTTON_WIDTH,
                        C.BUTTON_HEIGHT, "Create Query", C.BUTTON_DEFAULT_COLOUR,
                        C.CORNER_DEFAULT_COLOUR, Events.QUERY));

                widgets = s.getWidgets();

				screens.put(lastScreen++, s);
				currentScreen = lastScreen - 1;
				break;
			// Generates a Screen containing the results of a user query.
			case Events.QUERY:
				String query = Utility.generateQuery(Utility.generateConditions(widgets));
				ArrayList<DataContainer> d = DatabaseInterface.query(query);
				
				int maxPoints = 1000;
				ArrayList<DataContainer> data = new ArrayList<>(d.size() > maxPoints? maxPoints : d.size());
				for (int i = 0; i < maxPoints && i < d.size(); i++) {
					data.add(d.get(i));
				}

				Location nyLocation = new Location(40.74, -73.99);

				UnfoldingMap map = new UnfoldingMap(this, new Google.GoogleTerrainProvider());
				TaxiMapInterface.addJourneys(map, data, 1);

				map.zoomAndPanTo((C.MAX_ZOOM + C.MIN_ZOOM)/2, nyLocation);
				float maxPanningDistance = 40; // in km
				map.setPanningRestriction(nyLocation, maxPanningDistance);
				map.setZoomRange(C.MAX_ZOOM, C.MIN_ZOOM);

				Screen newUserQuery = new Screen(map);
				Screen queryGen = screens.get(lastScreen--);

				newUserQuery.add(backButton);
				newUserQuery.register();
				screens.put(lastScreen++, newUserQuery);
				screens.put(lastScreen++, queryGen);
				break;
			}
		}
	}

}
