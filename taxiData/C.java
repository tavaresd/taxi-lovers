package taxiData;

/**
 * C(onstants).java
 * @author D�rio Tavares Antunes
 * A class holding a few constants. Short name to shorten code.
 */
public class C/*onstants*/ {

	public static final int BUTTON_CORNER_WIDTH = 3;
	public static final int BUTTON_DEFAULT_COLOUR = 0xFF6464C8;
	public static final int CORNER_DEFAULT_COLOUR = 0xAAFF0000;
	public static final int FACE_OFFSET = 320;
	public static final int BUTTON_WIDTH = 150;
	public static final int BUTTON_HEIGHT = 44;
	public static final int BUTTON_MARGIN = BUTTON_HEIGHT + 10;
	public static final int MAX_ZOOM = 12;
	public static final int MIN_ZOOM = 16;

}
