package taxiData;

/**
 * Events.java
 * @author D�rio Tavares Antunes
 * A class holding the values of various event codes.
 */
public class Events {
	
	public static final int
	INVALID = Integer.MAX_VALUE,
	SCREEN = 0,
	NEW_SCREEN = -1,
	C1 = -2,
	C2 = -3,
	C3 = -4,
	C4 = -5,
	C5 = -6,
	C6 = -7,
	C7 = -8,
	C8 = -9,
	C9 = -10,
	C10 = -11,
	C11 = -12,
	C12 = -13,
	C13 = -14,
	C14 = -15,
	C15 = -16,
	C16 = -17,
	C17 = -18,
	MARK = -19,
    QUERY = -20;

}
