package taxiData;

import java.util.Arrays;

import de.fhpotsdam.unfolding.geo.Location;

/**
 * DataContainer.java
 * @author D�rio Tavares Antunes
 * @modified Joseph Fitzpatrick
 * @modified Patrick O'Boyle
 * @modified Dan Ormsby
 * Holds information about an entry.
 */
public class DataContainer {

	//static arrays containing possible values reduces memory usage
	private static String[] vendorIds = {"CMT", "VTS"};
	private static int[] rateCodes = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 28, 65, 128, 210};
	private static String[] storeAndFwdFlags = {"Y", "N", ""};

	private String medallion;
	private String licence;
	private boolean vendorId;
	private byte rateCode;
	private byte storeAndFwdFlag;
	private TripDuration tripTime;
	private int passengerCount;
	private int tripLength;
	private double tripDistance;
	private CoordinatePair pickupCoordinates;
	private CoordinatePair dropOffCoordinates;


	/**
	 * Creates a new DataContainer object. Only called by the DatabaseHandle
	 * class.
	 * @param medallion
	 * @param licence
	 * @param vendorId
	 * @param rateCode
	 * @param storeAndFwdFlag
	 * @param pickupTime
	 * @param dropOffTime
	 * @param passengerCount
	 * @param tripLength
	 * @param tripDistance
	 * @param pickupLatitude
	 * @param pickupLongitude
	 * @param dropOffLatitude
	 * @param dropOffLongitude
	 * @throws IllegalArgumentException
	 * Thrown if vendor id, rate code or store and forward flag are not valid. That
	 * is, not in the set of values stated to be valid.
	 * @author D�rio Tavares Antunes
	 */
	public DataContainer (String medallion, String licence, String vendorId, int rateCode,
			String storeAndFwdFlag, long pickupTime, long dropOffTime,
			int passengerCount, int tripLength, double tripDistance,
			double pickupLatitude, double pickupLongitude, double dropOffLatitude, 
			double dropOffLongitude) throws IllegalArgumentException {

		this.medallion = medallion;
		this.licence = licence;

		this.vendorId = vendorId.equals(vendorIds[0]);
		if (!this.vendorId) {
			if (!vendorId.equals(vendorIds[1])) {
				throw new IllegalArgumentException("Invalid vendor_id: " + vendorId);
			}
		}

		int rateIndex = Arrays.binarySearch(rateCodes, rateCode);
		if (rateIndex >= 0) {
			this.rateCode = (byte) rateIndex;
		} else {
			throw new IllegalArgumentException("Invalid rate_code: " + rateCode);
		}

		this.storeAndFwdFlag = -1;
		for (int i = 0; i < storeAndFwdFlags.length; i++) {
			if (storeAndFwdFlags[i].equals(storeAndFwdFlag)) {
				this.storeAndFwdFlag = (byte) i;
				break;
			}
		}
		if (this.storeAndFwdFlag == -1) {
			throw new IllegalArgumentException("Invalid store_and_fwd_flag: " + storeAndFwdFlag);
		}

		tripTime = new TripDuration(pickupTime, dropOffTime);
		this.passengerCount = passengerCount;
		this.tripLength = tripLength;
		this.tripDistance = tripDistance;
		pickupCoordinates = new CoordinatePair(pickupLatitude, pickupLongitude);
		dropOffCoordinates = new CoordinatePair(dropOffLatitude, dropOffLongitude);
	}


	//getters convert static array indices into correct values for convenience
	/**
	 * method getVendorId
	 * Converts the internal representation of the vendor id into a String
	 * for convenience.
	 * @author D�rio Tavares Antunes
	 */
	public String getVendorId() {
		return vendorId ? vendorIds[0] : vendorIds[1];
	}

	/**
	 * method getRateCode 
	 * Converts the internal representation of the rate code into an int
	 * for convenience.
	 * @author D�rio Tavares Antunes
	 */
	public int getRateCode() {
		return rateCodes[rateCode];
	}

	/**
	 * method getFlag 
	 * Converts the internal representation of the store and forward flag
	 * into a String for convenience.
	 * @author D�rio Tavares Antunes
	 */
	public String getFlag() {
		return storeAndFwdFlags[storeAndFwdFlag];
	}

	/**
	 * Auto-generated
	 * Returns the String representing the medallion.
	 */
	public String getMedallion() {
		return medallion;
	}

	/**
	 * Auto-generated
	 * Returns the String representing the licence.
	 */
	public String getLicence() {
		return licence;
	}

	/**
	 * Auto-generated
	 * Returns the TripDuration object containing the trip start and end times.
	 */
	public TripDuration getTripTime() {
		return tripTime;
	}

	/**
	 * Auto-generated
	 * Returns the int representing the passenger count.
	 */
	public int getPassengerCount() {
		return passengerCount;
	}

	/**
	 * Auto-generated
	 * Returns the int representing the trip duration in seconds.
	 */
	public int getTripLength() {
		return tripLength;
	}

	/**
	 * Auto-generated
	 * Returns the double representing the trip length in kilometres.
	 */
	public double getTripDistance() {
		return tripDistance;
	}

	/**
	 * Auto-generated
	 * Returns the CoordinatePair object containing the trip start coordinates.
	 */
	public CoordinatePair getPickupCoordinates() {
		return pickupCoordinates;
	}

	/**
	 * Auto-generated
	 * Returns the CoordinatePair object containing the trip end coordinates.
	 */
	public CoordinatePair getDropOffCoordinates() {
		return dropOffCoordinates;
	}
	
	/**
	 * 
	 * method getXLocation 
	 * @return
	 * The pick or drop off co-ordinates as a Location object for use in 
	 * unfolding maps.
	 * @author D�rio Tavares Antunes
	 */
	public Location getPickupLocation() {
		return new Location(pickupCoordinates.getLatitude(), pickupCoordinates.getLongitude());
	}
	
	public Location getDropOffLocation() {
		return new Location(dropOffCoordinates.getLatitude(), dropOffCoordinates.getLongitude());
	}

	/**
	 * method toString
	 * Returns a String detailing the contents of the DataContainer object.
	 * @author Joseph Fitzpatrick
	 * @modified Patrick O'Boyle
	 * @modified D�rio Tavares Antunes
	 * @modified Dan Ormsby
	 */
	public String toString(){
		String textOutput = "Vendor ID: " + getVendorId() + "\nRate Code: " + 
							getRateCode() + "\nFlag: "+ getFlag() +	"\nMedallion: "
							+ medallion + "\nTaxi Licence: " + licence + 
							"\nTrip Time: " + tripTime + "\nNumber of Passengers: "
							+ passengerCount +	"\nTrip Distance: " + 
							tripDistance + " km" + "\nPickUp Coordinates: " +
							pickupCoordinates + "\nDrop Off Coordinates: " + 
							dropOffCoordinates;
		return textOutput;
	}
}
