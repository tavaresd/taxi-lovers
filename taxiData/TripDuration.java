package taxiData;

import java.sql.Timestamp;

/**
 * TripDuration.java
 * @author D�rio Tavares Antunes
 * @modified Sean O'Carroll
 * Holds information about a pickup and dropoff time.
 */
public class TripDuration {

	private Timestamp pickupTime;
	private Timestamp dropOffTime;

	/**
	 * Creates a new TripDuration object.
	 * @author D�rio Tavares Antunes
	 */
	public TripDuration(String pickupTime, String dropOffTime) {
		this.pickupTime = Timestamp.valueOf(pickupTime);
		this.dropOffTime = Timestamp.valueOf(dropOffTime);
	}
	
	public TripDuration(long pickupTime, long dropOffTime) {
		this.pickupTime = new Timestamp(pickupTime);
		this.dropOffTime = new Timestamp(dropOffTime);
	}

	/**
	 *	Auto-generated
	 *	Returns a Timestamp object representing the time the pickup occurred. 
	 */
	public Timestamp getPickupTime() {
		return pickupTime;
	}

	/**
	 *	Auto-generated
	 *	Returns a Timestamp object representing the time the drop off occurred. 
	 */
	public Timestamp getDropOffTime() {
		return dropOffTime;
	}
	
	/**
	 * method toString
	 * Returns a String representation of this object, for use in DataContainer's
	 * toString method.
	 * @author Sean O'Carroll
	 */
	public String toString() {
		return ("Pickup Time: " + getPickupTime() + ", Dropoff Time: " + getDropOffTime());
	}

}
