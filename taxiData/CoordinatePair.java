package taxiData;

/**
 * CoordinatePair.java
 * @author D�rio Tavares Antunes
 * @modified Sean O'Carroll
 * @modified Dan Ormsby
 * Holds a latitude/longitude pair for a pickup or drop off location
 */
public class CoordinatePair implements Comparable<CoordinatePair> {

	private double latitude;
	private double longitude;

	/**
	 * Creates a CoordinatePair object with the given latitude and longitude.
	 * @param latitude
	 * @param longitude
	 * @author D�rio Tavares Antunes
	 */
	public CoordinatePair(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	/**
	 * Auto-generated
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * Auto-generated
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * method toString
	 * Returns a String representation of this object.
	 * @author Sean O'Carroll
	 * @modified Dan Ormsby
	 */
	public String toString() {
		return ("Latitude: " + latitude + "�" +  " Longitude: " + longitude + "°");
	}

	@Override
	/**
	 * method compareTo
	 * Used in the convex hull generating algorithm.
	 * @author Wikipedia Contributor
	 * @modified D�rio Tavares Antunes
	 */
	public int compareTo(CoordinatePair o) {
		if (this.longitude == o.longitude) {
			return (int) ((this.latitude - o.latitude) * 1000000d);
		} else {
			return (int) ((this.longitude - o.longitude) * 1000000d);
		}
	}

}
